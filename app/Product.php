<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products"; //Nếu không khai báo thì Laravel sẽ mặc định nhận tên Class và có chữ "s" ở đằng sau. Vd: Class Product => table: Products

    public function product_type()
    {
        return $this->belongsTo('App\ProductType', 'id_type', 'id');
    }

    public function bill_detail()
    {
        return $this->hasMany('App\BillDetail', 'id_product', 'id');
    }
}
